using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController : ControllerBase
    {
        private readonly PaymentContext _context;
        public ProdutoController(PaymentContext context)
        {
            _context = context;
        }
        
        [HttpPost("CriarProduto")]
        public IActionResult Criar(Produto produto)
        {
            _context.Add(produto);
            _context.SaveChanges();
            return Ok(produto);
        }

        [HttpGet("ObterTodos")]
        public IActionResult ObterTodos()
        {
            var produtos = _context.Produtos.ToList();
            return Ok(produtos);
        }

        [HttpGet("ObterProduto/{id}")]
        public IActionResult ObterProduto(int id)
        {
            var produto = _context.Produtos.Find(id);

            if (produto == null)
            {
                return NotFound();
            }

            return Ok(produto);
        }

        [HttpPut("Atualizar/{id}")]
        public IActionResult Atualizar(int id, Produto produto)
        {
            var produtoBanco = _context.Produtos.Find(id);

            if (produtoBanco == null)
            {
                return NotFound();
            }

            produtoBanco.Nome = produto.Nome;

            _context.Produtos.Update(produtoBanco);
            _context.SaveChanges();
            return Ok(produtoBanco);
        }
        
        [HttpDelete("Deletar/{id}")]
        public IActionResult Deletar(int id)
        {
            var produtoBanco = _context.Produtos.Find(id);

            if (produtoBanco == null)
            {
                return NotFound();
            }

            _context.Produtos.Remove(produtoBanco);
            _context.SaveChanges();
            return NoContent();
        }
    }
}