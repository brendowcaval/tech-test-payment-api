using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly PaymentContext _context;

        public VendedorController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost("CriarVendedor")]
        public IActionResult Criar(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);
        }


        [HttpGet("ObterVendedor/{id}")]
        public IActionResult ObterVendedor(int id)
        {
            var vendedor = _context.Vendedores.Find(id);

            if (vendedor == null)
            {
                NotFound();
            }

            return Ok(vendedor);
        }

        [HttpPut("Atualizar/{id}")]
        public IActionResult Atualizar(int id, Vendedor vendedor)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if (vendedorBanco == null)
            {
                return NotFound();
            }

            vendedorBanco.Cpf = vendedor.Cpf;
            vendedorBanco.Nome = vendedor.Nome;
            vendedorBanco.Email = vendedor.Email;
            vendedorBanco.Telefone = vendedor.Telefone;
            _context.Vendedores.Update(vendedorBanco);
            _context.SaveChanges();
            return Ok(vendedorBanco);

        }


        [HttpDelete("Deletar/{id}")]
        public IActionResult Deletar(int id)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if (vendedorBanco == null)
            {
                return NotFound();
            }

            _context.Vendedores.Remove(vendedorBanco);
            _context.SaveChanges();
            return NoContent();

        }
        
    }
}