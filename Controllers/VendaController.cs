using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly PaymentContext _context;

        public VendaController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost("RegistrarVenda/{idProduto}/{idVendedor}")]
        public IActionResult RegistrarVenda(Venda venda, int idProduto, int idVendedor)
        {
             

            _context.Add(venda);
            // Encontrar se existe os ids em produto e vendedor
            var idP = _context.Produtos.Find(idProduto); 
            var idV = _context.Vendedores.Find(idVendedor);

            if (idP == null || idV == null)
            {
                return NotFound();
            }
            
            // se ids existem, são preenchidos nos dados
            venda.IdProduto = idProduto;
            venda.IdVendedor = idVendedor;
            venda.Status = "Aguardando Pagamento";
            _context.SaveChanges();
            return Ok(venda);

        }

        [HttpGet("ObterVenda/{id}")]
        public IActionResult ObterVenda(int id)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
            {
                return NotFound();
            }

            return Ok(venda);
        }
        
        [HttpPut("Atualizar/{id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
            {
                return NotFound();
            }

            vendaBanco.Status = venda.Status;
            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();
            return Ok(vendaBanco);
        }


        [HttpDelete("Deletar/{id}")]
        public IActionResult Deletar(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
            {
                return NotFound();
            }

            _context.Vendas.Remove(vendaBanco);
            _context.SaveChanges();
            return NoContent();

        }

        
    
        
    }
}